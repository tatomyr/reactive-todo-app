const version = 'Fri 13 May 2022 20:29:26 CEST'
const cacheName = version
let srcFiles = ['/config/images.js','/purity-modules/core.js','/purity-modules/debounce.js','/purity-modules/debounce.min.js','/purity-modules/delay.js','/purity-modules/delay.min.js','/purity-modules/md5.js','/purity-modules/pipe.js','/purity-modules/register-async.js','/purity-modules/register-async.min.js','/purity-modules/register-async.test.js','/purity-modules/sanitize.js','/purity-modules/sanitize.min.js','/purity-modules/visibility-sensor.js','/purity-modules/visibility-sensor.min.js','/services/actions.js','/services/fetch-images.js','/services/filters.js','/services/google-api.js','/services/image-processing.js','/services/index.js','/services/local-storage.js','/services/tasks-manager.js','/services/text-files-manager.js','/services/text-format.js','/store/action-types.js','/store/async-handler.js','/store/provider.js','/store/state-handler.js','/ui/App.js','/ui/components/Notification.js','/ui/components/InputForm.js','/ui/components/TaskItem.js','/ui/components/NavBar.js','/ui/components/TaskDetailsControls.js','/ui/components/TaskDetails.js','/ui/components/NavAboutButton.js','/ui/components/Icon.js','/ui/components/NavItem.js','/ui/components/index.js','/ui/components/TaskItemControls.js','/ui/components/TasksList.js','/ui/components/Bubble.js','/ui/pages/index.js','/ui/pages/TodoPage.js','/ui/pages/StartPage.js','/ui/pages/InfoPage.js',] // Should come from bash script when building
// FIXME:
let filesToCache = [
  '/',
  '/index.html',
  '/index.js',
  '/manifest.json',
  '/styles/reset.css',
  '/styles/style.css',
  '/assets/images/loading-shape.gif', // IMAGES.LOADING
  '/assets/images/icon-pack/idea.svg', // IMAGES.UNDEFINED_TASK
  '/assets/images/icon-pack/forbidden.svg', // IMAGES.BROKEN
  // Modules
  './purity-modules/core.js',
  './purity-modules/register-async.js',
  './purity-modules/debounce.js',
  './purity-modules/md5.js',
  './purity-modules/sanitize.js',
  // Fonts
  'https://fonts.googleapis.com/css?family=Fira+Sans',
  'https://tatomyr.github.io/unisource/unisource.ttf',
]

if (srcFiles) {
  console.log(srcFiles)
  filesToCache = [...filesToCache, ...srcFiles]
}

self.addEventListener('install', e => {
  console.log('[ServiceWorker] Install')
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      console.log('[ServiceWorker] Caching app shell')
      return cache.addAll(filesToCache)
    })
  )
})

self.addEventListener('activate', e => {
  console.log('[ServiceWorker] Activate')
  e.waitUntil(
    caches.keys().then(keyList =>
      Promise.all(
        keyList.map(key => {
          if (key !== cacheName) {
            console.log('[ServiceWorker] Removing old cache', key)
            return caches.delete(key)
          }
          return undefined
        })
      )
    )
  )
  return self.clients.claim()
})

self.addEventListener('fetch', e => {
  console.log('[Service Worker] Fetch', e.request.url)
  e.respondWith(caches.match(e.request).then(res => res || fetch(e.request)))
})
